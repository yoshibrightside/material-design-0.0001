package mx.unam.fciencias.yocho.materialdesign;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @SuppressWarnings("unused")
    public void openInfiniteList(View openButton) {
        Intent starter = new Intent(this, SecondActivity.class);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.recycler_button) {
                openInfiniteList(view);
        }
    }
}